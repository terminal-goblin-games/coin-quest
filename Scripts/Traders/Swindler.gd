extends "res://Scripts/Trader.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	gold_cost_mult = 1
#	$VBoxContainer/Prog_Bar.set_bar((int(prod_time) - int($Timer.time_left)), prod_time)
#	$Timer.start()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$VBoxContainer/Prog_Bar.set_bar((int(prod_time) - int($Timer.time_left)), prod_time)
	$VBoxContainer/Buy.text = "Buy " + str(gold_purchase_price) + "G"

func inc_count():
	unit_count += 1
	update_formulas()
	
func _on_Timer_timeout():
#	var tg = tg_income_per_tick * unit_count
#	var g = gold_income_per_tick * unit_count
#	var s = strat_income_per_tick * unit_count
#	var o = opp_income_per_tick * unit_count
#	print("Trader gold income: " + str(g))
#	get_node("/root/Main").resource_incr(tg,g,s,o)
#	get_node("/root/Main").update_resources()
	actual_sales_vol = min((unit_sales_volume * unit_count * sv_mult), get_node("/root/Main").trade_goods)
	var tg = tg_income_per_tick
	var g = gold_income_per_tick
	var s = strat_income_per_tick
	var o = opp_income_per_tick
	get_node("/root/Main").resource_incr(tg,g,s,o)
	get_node("/root/Main").update_resources()
	get_node("/root/Main").trade_goods -= actual_sales_vol
	get_node("/root/Main").update_resources()
	print("Gold cost: " + str(gold_purchase_price))
	print("Gold income: " + str(g))


func _on_Buy_pressed():
	if get_node("/root/Main").gold >= gold_purchase_price:
		if unit_count == 0:
			$VBoxContainer/Prog_Bar.set_bar((int(prod_time) - int($Timer.time_left)), prod_time)
			$Timer.start()
			get_node("/root/Main").current_trader_units += 1
			get_node("/root/Main").update_resources()
		inc_count()
		get_node("/root/Main").current_trader_units += 1
		get_node("/root/Main").gold -= gold_purchase_price
		get_node("/root/Main").update_resources()
		$VBoxContainer/HBoxContainer/VBoxContainer/Income/Gold/Label.text = str(tg_income_per_tick * unit_count)
	else:
		print("Can't afford it")
