extends "res://Scripts/Battler.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	gold_cost_mult = 2
	$VBoxContainer/Prog_Bar.set_bar((int(prod_time) - int($Timer.time_left)), prod_time)
	$Timer.start()
	get_node("/root/Main").update_resources()

func inc_count():
	unit_count += 1
	get_node("/root/Main").max_battler_units += 10
	update_formulas()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$VBoxContainer/Prog_Bar.set_bar((int(prod_time) - int($Timer.time_left)), prod_time)
func _on_Timer_timeout():
	var tg = tg_income_per_tick * unit_count
	var g = gold_income_per_tick * unit_count
	var s = strat_income_per_tick * unit_count
	var o = opp_income_per_tick * unit_count
	get_node("/root/Main").resource_incr(tg,g,s,o)
	get_node("/root/Main").update_resources()
