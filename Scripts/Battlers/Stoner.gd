extends "res://Scripts/Battler.gd"

var floating_text = preload("res://Scenes/Floating_Text.tscn")

func _init():
	unit_count = 0
	tg_rev_mult = 1
	gold_rev_mult = 0 
	strat_rev_mult = 0
	opp_rev_mult = 0
	
func _ready():
	pass
#	$VBoxContainer/Prog_Bar.set_bar((int(prod_time) - int($Timer.time_left)), prod_time)
#	$Timer.start()
#	get_node("/root/Main").update_resources()
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$VBoxContainer/Prog_Bar.set_bar((int(prod_time) - int($Timer.time_left)), prod_time)
	$VBoxContainer/HBoxContainer/VBoxContainer/Quantity.text = "# " + str(unit_count)
	$VBoxContainer/Buy.text = "Buy " + str(gold_purchase_price) + " G"

func _on_Timer_timeout():
	var tg = tg_income_per_tick * unit_count
	var g = gold_income_per_tick * unit_count
	var s = strat_income_per_tick * unit_count
	var o = opp_income_per_tick * unit_count
	print("Stoner tg income: " + str(tg))
	get_node("/root/Main").resource_incr(tg,g,s,o)
	get_node("/root/Main").update_resources()

func _on_Buy_button_down():
	if get_node("/root/Main").gold >= gold_purchase_price:
		if unit_count == 0:
			$VBoxContainer/Prog_Bar.set_bar((int(prod_time) - int($Timer.time_left)), prod_time)
			$Timer.start()
			get_node("/root/Main").current_battler_units += 1
			get_node("/root/Main").update_resources()
		inc_count()
		get_node("/root/Main").current_battler_units += 1
		get_node("/root/Main").gold -= gold_purchase_price
		get_node("/root/Main").update_resources()
		$"VBoxContainer/HBoxContainer/VBoxContainer/Income/Trade Goods/Label".text = str(tg_income_per_tick * unit_count)
	else:
		print("Can't afford it")
