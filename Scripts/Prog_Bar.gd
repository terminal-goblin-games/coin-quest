extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func set_bar(val, prod_time):
	self.value = val
	$Label.text = str(val) +"/" + str(prod_time)
	self.max_value = prod_time
