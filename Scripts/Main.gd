extends Node2D

enum states {BATTLEFIELD, TRADING}
var state = states.BATTLEFIELD

# Setup for units
var officer_sprite = preload("res://Sprites/battler_officer_L1.png")
var merch_sprite = preload("res://Sprites/merchant.png")
var time = preload("res://Scenes/Click_Timer.tscn")
var p_trader = preload("res://Scenes/Trade_Officer_L1.tscn")
var p_battler = preload("res://Scenes/Battler_Officer_L1.tscn")#preload("res://Scenes/Battler.tscn")
var n_battler = null
var n_trader = null
var p_stoner = preload("res://Scenes/Battlers/Stoner.tscn")
var n_stoner = null
var p_swindler = preload("res://Scenes/Traders/Swindler.tscn")
var n_swindler = null

# Resources
var gold = 1000
var trade_goods = 0
var strat = 0
var opp = 0
var max_battler_units = 5
var current_battler_units = 0
var trade_goods_increment = 10
var gold_increment = 1
var max_trader_units = 5
var current_trader_units = 0

var floating_text = preload("res://Scenes/Floating_Text.tscn")
var inital_officer_price = 500
var initial_unit_price = 50

# Called when the node enters the scene tree for the first time.
func _ready():
	$Gui/Resources/HBoxContainer/Gold.text = "Gold: " + str(gold)
	$Gui/Sidebar/Battlefield.visible = true
	update_stats()
	update_resources()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	state_machine()

func state_machine():
	match state:
		states.BATTLEFIELD:
			$Gui/Battler_Icon_Container.visible = true
			$Gui/Trade.visible = false
			$Gui/Trader_Icon_Container.visible = false
			$Gui/Fight.visible = true
			#$Gui/Enemy.visible = true
			$Gui/Location.text = "Battlefield"
			$Gui/Battler_Icon_Container.visible = true
		states.TRADING:
			#$Gui/Enemy.visible = false
			$Gui/Trader_Icon_Container.visible = true
			$Gui/Battler_Icon_Container.visible = false
			$Gui/Location.text = "Trading Post"
			$Gui/Fight/.visible = false
			$Gui/Trade.visible = true
			$Gui/Battler_Icon_Container.visible = false

func _on_Trade_pressed():
	if trade_goods != 0:
		gold_incr(gold_increment)
		trade_goods -= 1
		update_resources()
	else:
		print("Nothing to trade")

func gold_incr(inc):
	if trade_goods != 0:
		gold += 50
		update_resources()

func resource_incr(t_inc, g_inc, s_inc, o_inc):
	trade_goods += t_inc
	gold += g_inc
	strat += s_inc
	opp += o_inc

func update_resources():
	$Gui/Resources/HBoxContainer/Gold.text = "Gold: " + comma_sep(gold)
	$Gui/Resources/HBoxContainer2/Trade_Goods.text = "Trade Goods: " + comma_sep(trade_goods)
	$Gui/Resources/HBoxContainer3/Strategy.text = "Strategy: " + comma_sep(strat)
	$Gui/Resources/HBoxContainer4/Opportunity.text = "Opportunity: " + comma_sep(opp)
#	$Gui/Battler_Resources/Max_Battlers.text = "Max Battlers: " + comma_sep(max_battler_units)
#	$Gui/Battler_Resources/Max_Traders.text = "Max Traders " + comma_sep(max_trader_units)
	$Gui/Battler_Resources/Battler_Units.text = "Battlers: " + comma_sep(current_battler_units) + "/" + comma_sep(max_battler_units)
	$Gui/Battler_Resources/Trader_Units.text = "Traders: " + comma_sep(current_trader_units) + "/" + comma_sep(max_trader_units)

func update_stats():
	$Gui/Stats_popup/Stats/Gold_Per_Click.text = "Gold per click: " + str(gold_increment)
#	$Gui/Stats_popup/Stats/Trade_Goods_Per_Click.text = "Trade Goods per fight: : " + str(auto_increment)
	#$Gui/Stats_popup/Stats/Trader_Count.text = "Traders: " + str(trader_count)
	#$Gui/Stats_popup/Stats/Battler_Count.text = "Battlers: " + str(battler_count)

func _on_Upgrades_pressed():
	$Gui/Upgrade_Menu.show()


func _on_Trade_Officer_pressed():
	if get_node("/root/Main/Gui/Trade/Trade_Officer_L1") == null && gold >= inital_officer_price:
		n_trader = p_trader.instance()
		gold -= 50
		$Gui/Trade.add_child(n_trader)
		$Gui/Trade/Trade.visible = false
		get_node("/root/Main/Gui/Trade/Trade_Officer_L1").inc_count()
		update_stats()
		update_resources()
		#$Gui/Trade.texture_normal = merch_sprite
	elif gold >= get_node("/root/Main/Gui/Trade/Trade_Officer_L1").gold_purchase_price:
		gold -= get_node("/root/Main/Gui/Trade/Trade_Officer_L1").gold_purchase_price
		get_node("/root/Main/Gui/Trade/Trade_Officer_L1").inc_count()
		update_resources()
	else:
		print("costs: " + str(get_node("/root/Main/Gui/Trade/Trade_Officer_L1").gold_purchase_price))

func _on_Show_Stats_pressed():
	$Gui/Stats_popup.show()

func _on_Battle_Officer_pressed():
	if get_node("/root/Main/Gui/Fight/Battler_Officer_L1") == null && gold >= inital_officer_price:
		gold -= 10
		n_battler = p_battler.instance()
		$Gui/Fight.add_child(n_battler)
		$Gui/Fight/Fight.visible = false
		get_node("/root/Main/Gui/Fight/Battler_Officer_L1").inc_count()
		#$Gui/Fight.texture_normal = officer_sprite
		update_resources()
	elif gold >= get_node("/root/Main/Gui/Fight/Battler_Officer_L1").gold_purchase_price:
		gold -= get_node("/root/Main/Gui/Fight/Battler_Officer_L1").gold_purchase_price
		print("Battler cost: " + str(get_node("/root/Main/Gui/Fight/Battler_Officer_L1").gold_purchase_price))
		get_node("/root/Main/Gui/Fight/Battler_Officer_L1").inc_count()
		update_resources()
	else:
		print("Can't afford it")

func _on_Battlefield_pressed():
	state = states.BATTLEFIELD

func _on_Trade_Post_pressed():
	state = states.TRADING

func _on_Fight_pressed():
	var text = floating_text.instance()
	text.amount = 1
	get_node("Gui/Fight").add_child(text)
	resource_incr(1,0,0,0)
	update_resources()

func comma_sep(number):
	var string = str(number)
	var mod = string.length() % 3
	var res = ""

	for i in range(0, string.length()):
		if i != 0 && i % 3 == mod:
			res += ","
		res += string[i]

	return res


func _on_Stoner_pressed():
	if get_node("Gui/Battler_Icon_Container/Stoner") == null && current_battler_units < max_battler_units && gold >= initial_unit_price:
		n_stoner = p_stoner.instance()
		$Gui/Battler_Icon_Container.add_child(n_stoner)
		current_battler_units += 1
		gold -= 50
		update_resources()
	elif gold > get_node("Gui/Battler_Icon_Container/Stoner").gold_purchase_price && current_battler_units < max_battler_units:
		current_battler_units += 1
		get_node("Gui/Battler_Icon_Container/Stoner").inc_count()
		gold -= get_node("Gui/Battler_Icon_Container/Stoner").gold_purchase_price
		update_resources()


func _on_Swindler_pressed():
	if get_node("Gui/Trader_Icon_Container/Swindler") == null && current_trader_units < max_trader_units && gold >= initial_unit_price:
		n_swindler = p_swindler.instance()
		$Gui/Trader_Icon_Container.add_child(n_swindler)
		current_trader_units += 1
		gold -= 50
		update_resources()
	elif gold > get_node("Gui/Trader_Icon_Container/Swindler").gold_purchase_price && current_trader_units < max_trader_units:
		current_trader_units += 1
		get_node("Gui/Trader_Icon_Container/Swindler").inc_count()
		gold -= get_node("Gui/Trader_Icon_Container/Swindler").gold_purchase_price
		update_resources()


func _on_Back_pressed():
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Battlers.visible = true
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Traders.visible = true
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Officers.visible = true
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Battlers_box.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Traders_box.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Officers_box.visible = false



func _on_Battlers_pressed():
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Battlers.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Traders.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Officers.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Battlers_box.visible = true


func _on_Traders_pressed():
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Battlers.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Traders.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Officers.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Traders_box.visible = true


func _on_Officers_pressed():
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Battlers.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Traders.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Officers.visible = false
	$Gui/Upgrade_Menu/ScrollContainer/VBoxContainer/Officers_box.visible = true
