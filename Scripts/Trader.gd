extends "res://Scripts/Unit.gd"

var unit_sales_volume = 1
var sv_mult = 1.0
# Init func is where we override vars from the parent script
func _init():
	unit_count = 0
	tg_rev_mult = 0
	gold_rev_mult = 1 
	strat_rev_mult = 0
	opp_rev_mult = 2
	actual_sales_vol = 0

func _ready():
	$Timer.wait_time = prod_time
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#get_node("/root/Main/Gui/Trader_Prog_Bar").set_bar((int(prod_time) - int($Timer.time_left)), prod_time)
	update_formulas()

func inc_count():
	unit_count += 1
	update_formulas()

func _on_Timer_timeout():
	#if get_node("/root/Main").trade_goods >= unit_count * 10:
	actual_sales_vol = min((unit_sales_volume * unit_count * sv_mult), get_node("/root/Main").trade_goods)
	var tg = tg_income_per_tick
	var g = gold_income_per_tick
	var s = strat_income_per_tick
	var o = opp_income_per_tick
	get_node("/root/Main").resource_incr(tg,g,s,o)
	get_node("/root/Main").update_resources()
	get_node("/root/Main").trade_goods -= actual_sales_vol
	get_node("/root/Main").update_resources()
	print("Gold cost: " + str(gold_purchase_price))
	print("Gold income: " + str(g))

func _on_Battler_pressed():
	var tg = tg_rev
	var g = gold_rev
	var s = strat_rev
	var o = opp_rev
	get_node("/root/Main").resource_incr(tg,g,s,o)
	get_node("/root/Main").update_resources()
