extends "res://Scripts/Unit.gd"

# Init func is where we override vars from the parent script
func _init():
	unit_count = 0
	tg_rev_mult = 1
	gold_rev_mult = 0 
	strat_rev_mult = 2
	opp_rev_mult = 0
func _ready():
	$Timer.wait_time = prod_time
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#get_node("/root/Main/Gui/Battler_Prog_Bar").set_bar((int(prod_time) - int($Timer.time_left)), prod_time)
	update_formulas()

func inc_count():
	unit_count += 1
	update_formulas()

func _on_Timer_timeout():
	var tg = tg_income_per_tick * unit_count
	var g = gold_income_per_tick * unit_count
	var s = strat_income_per_tick * unit_count
	var o = opp_income_per_tick * unit_count
	get_node("/root/Main").resource_incr(tg,g,s,o)
	get_node("/root/Main").update_resources()


func _on_Battler_pressed():
	var tg = tg_rev * unit_count
	var g = gold_rev * unit_count
	var s = strat_rev * unit_count
	var o = opp_rev * unit_count
	get_node("/root/Main").resource_incr(tg,g,s,o)
	get_node("/root/Main").update_resources()
