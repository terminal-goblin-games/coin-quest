extends TextureButton

################
# Description  
# 
# This is a generic Unit class and contains the formulas and multipliers that every unit that "produces"
# things will use
#
#################

# Generic vars for formulas
var unit_count = 0
var prod_time = 10


################
# Formulas     
################

# Gold formula
var gold_base_cost = 50
var gold_cost_mult = 1
var gold_coefficient = 2.0
var gold_prod_time_mult = 1
var gold_rev = 100
var gold_rev_mult = 1
var actual_sales_vol = 0
var gold_cost_formula = (gold_base_cost * gold_cost_mult) * (pow(gold_coefficient,float(unit_count)))
var gold_rev_formula = (actual_sales_vol * (gold_rev * gold_rev_mult)) / (prod_time * gold_prod_time_mult)
var gold_income_per_tick = gold_rev * gold_rev_mult
var gold_purchase_price = gold_cost_formula

# Trade Goods formula
var tg_base_cost = 50
var tg_cost_mult = 1
var tg_coefficient = 2.0
var tg_prod_time_mult = 1
var tg_rev = 10
var tg_rev_mult = 1
var tg_cost_formula = (tg_base_cost * tg_cost_mult) * (pow(tg_coefficient,float(unit_count)))
var tg_rev_formula = (unit_count * (gold_rev * tg_rev_mult)) / (prod_time * tg_prod_time_mult)
var tg_income_per_tick = tg_rev_formula
var tg_purchase_price = tg_cost_formula

# Strategy formula
var strat_base_cost = 100
var strat_cost_mult = 1
var strat_coefficient = 2.0
var strat_prod_time_mult = 1
var strat_rev = 10
var strat_rev_mult = 1
var strat_cost_formula = (strat_base_cost * strat_cost_mult) * (pow(strat_coefficient,float(unit_count)))
var strat_rev_formula = (unit_count * (strat_rev * strat_rev_mult)) / (prod_time * strat_prod_time_mult)
var strat_income_per_tick = strat_rev_formula
var strat_purchase_price = strat_cost_formula

# Opportunity formula
var opp_base_cost = 100
var opp_cost_mult = 1
var opp_coefficient = 2.0
var opp_prod_time_mult = 1
var opp_rev = 1
var opp_rev_mult = 1
var opp_cost_formula = (opp_base_cost * opp_cost_mult) * (pow(opp_coefficient,float(unit_count)))
var opp_rev_formula = (unit_count * (opp_rev * opp_rev_mult)) / (prod_time * opp_prod_time_mult)
var opp_income_per_tick = opp_rev_formula
var opp_purchase_price = opp_cost_formula
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func update_formulas():
	# Gold
	gold_cost_formula = (gold_base_cost * gold_cost_mult) * (pow(gold_coefficient,float(unit_count)))
	gold_rev_formula = (actual_sales_vol * (gold_rev * gold_rev_mult))
	gold_income_per_tick = gold_rev_formula
	gold_purchase_price = gold_cost_formula
	
	# Trade Goods
	tg_cost_formula = (tg_base_cost * tg_cost_mult) * (pow(tg_coefficient,float(unit_count)))
	tg_rev_formula = (unit_count * (tg_rev * tg_rev_mult))
	tg_income_per_tick = tg_rev_formula
	tg_purchase_price = tg_cost_formula
	
	# Strat
	strat_cost_formula = (strat_base_cost * strat_cost_mult) * (pow(strat_coefficient,float(unit_count)))
	strat_rev_formula = (unit_count * (strat_rev * strat_rev_mult))
	strat_income_per_tick = strat_rev_formula
	strat_purchase_price = strat_cost_formula
	
	# Oppurtunity
	opp_cost_formula = (opp_base_cost * opp_cost_mult) * (pow(opp_coefficient,float(unit_count)))
	opp_rev_formula = (unit_count * (opp_rev * opp_rev_mult))
	opp_income_per_tick = opp_rev_formula
	opp_purchase_price = opp_cost_formula

func units():
	var strang = "Unit.gd unit_count: " + str(unit_count)
	return strang
