extends Timer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	self.wait_time = get_parent().click_freq


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Auto_Click_timeout():
	get_parent().incr((get_parent().auto_increment * get_parent().timer_count))
	get_parent().update_points()
